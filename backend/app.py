from flask import Flask, jsonify, request, send_file, render_template
import json
import duckdb
import os
import atexit
import uuid


app = Flask(__name__)


# Install DuckDB extensions
conn = duckdb.connect()
conn.execute("INSTALL spatial")
conn.execute("INSTALL httpfs")
conn.execute("""
LOAD spatial;
LOAD httpfs;
SET s3_region='us-west-2';
""")


# Register a cleanup function using atexit
def cleanup():
    print("Cleaning up before app exits...")
    conn.close()
    
    try:
        os.remove('buildings_sample.geojson')
    except:
        None
 
atexit.register(cleanup)
 
        
def request_signature(request):
    return f"{str(uuid.uuid4())}"

def query_columns(theme):
    if theme == 'buildings':
        return '''
        type,
        version,
        CAST(updatetime as varchar) as updateTime,
        height,
        numfloors as numFloors,
        level,
        class,
        JSON(names) as names,
        JSON(sources) as sources,
        ST_GeomFromWKB(geometry) as geometry
        '''
    
    if theme == 'admins':
        return '''
        type,
        version, 
        subType,
        localityType,
        context,
        maritime,
        adminLevel,
        isoCountryCodeAlpha2,
        JSON(names) AS names,
        CAST(updatetime as varchar) as updateTime,
        JSON(sources) AS sources,
        ST_GeomFromWkb(geometry) AS geometry
        '''
    
    if theme == 'places':
        return '''
        type,
        version, 
        JSON(names) AS names,
        JSON(categories) AS categories,
        JSON(confidence) AS confidence,
        JSON(websites) AS websites,
        JSON(socials) AS socials,
        JSON(emails) AS emails,
        JSON(phones) AS phones,
        JSON(brand) AS brand,
        JSON(addresses) AS addresses,
        round(confidence,2) as confidence,
        CAST(updatetime as varchar) as updateTime,
        JSON(sources) AS sources,
        ST_GeomFromWkb(geometry) AS geometry
        '''
    
    if theme == 'transportation':
        return '''
        type,
        subType,
        version,
        JSON(connectors) AS connectors,
        CAST(updatetime as varchar) as updateTime,
        JSON(sources) AS sources,
        ST_GeomFromWkb(geometry) AS geometry
    '''
        

def query_filter(xmin, ymin, xmax, ymax, classType, minHeight, category, confidence, subType, localityType):
    filter = ""
    if xmin is not None and ymin is not None and xmax is not None and ymax is not None:
        filter = f'''
        bbox.minx > {xmin}
        AND bbox.maxx < {xmax} 
        AND bbox.miny > {ymin}
        AND bbox.maxy < {ymax}
    '''

    if classType is not None:
        filter = filter + f'''
        AND class = '{classType}'
    '''

    if minHeight is not None:
        filter = filter + f'''
        AND height > {minHeight}
    '''

    if category is not None:
        filter = filter + f'''
        AND categories = '{category}'
    '''
    
    if confidence is not None:
        filter = filter + f'''
        AND confidence > {confidence}
    '''
    
    if subType is not None:
        filter = filter + f'''
        AND subType = '{subType}'
    '''
    
    if localityType is not None:
        filter = filter + f'''
        AND localityType = '{localityType}'
    '''
    
    if filter != "":
        filter = "WHERE " + filter
    
    return filter



@app.route('/map', methods=['GET'])
def show_map():
    return render_template('map.html')

@app.route('/data', methods=['GET'])
def get_geospatial_data():
    s3_base_url = 's3://overturemaps-us-west-2/release/2023-07-26-alpha.0'
    
    
    theme = request.args.get('theme', default='buildings', type=str).lower()
    limit = request.args.get('limit', default=10, type=int)
    
    xmin = request.args.get('xmin', type=float)
    ymin = request.args.get('ymin', type=float)
    xmax = request.args.get('xmax', type=float)
    ymax = request.args.get('ymax', type=float)
    classType = request.args.get('class', type=str)
    minHeight = request.args.get('minHeight', type=float)
    category = request.args.get('category', type=str)
    confidence = request.args.get('confidence', type=float)
    subType = request.args.get('subType', type=str)
    localityType = request.args.get('localityType', type=str)
    
    valid_themes = ['admins', 'places', 'buildings', 'transportation']
    if theme not in valid_themes:
        return jsonify({'error': 'Invalid theme parameter'}), 400
    
    signature = request_signature(request)
    print(f"Request {signature} fetching {limit} rows from {theme}")
    
    # Read Parquet file using DuckDB
    query = f'''
    COPY (
    SELECT
        {query_columns(theme)}
    FROM
        read_parquet('{s3_base_url}/theme={theme}/type=*/*', hive_partitioning=1)
    {query_filter(xmin, ymin, xmax, ymax, classType, minHeight, category, confidence, subType, localityType)}
    LIMIT
    {limit}
    
    ) TO '{signature}.geojson'
    WITH (FORMAT GDAL, DRIVER 'GeoJSON');
    '''
    
    conn.execute(query)
    
    print('Sending GeoJSON file...')
    try:
        return send_file(f'{signature}.geojson', mimetype='application/json', as_attachment=False)
    except Exception as e:
        return str(e)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
