interface Building {
    geometry: {
      type: 'Polygon' | 'MultiPolygon';
      coordinates: number[][][] | number[][][][];
    };
    properties: {
      names?: string[];
      height?: number;
      numFloors?: number;
      class?: 'residential' | 'outbuilding' | 'agricultural' | 'commercial' | 'industrial' | 'education' | 'service' | 'religious' | 'civic' | 'transportation' | 'medical' | 'entertainment' | 'military';
    };
  }

  interface Place {
    geometry: {
      type: 'Point';
      coordinates: number[];
    };
    properties: {
      names?: string[];
      categories: {
        main: string;
        alternate?: string[];
      };
      confidence?: number;
      websites?: string[];
      socials?: string[];
      emails?: string[];
      phones?: string[];
      brand?: {
        names?: string[];
        wikidata?: string;
      };
      addresses?: string[];
    };
  }

  
  interface Connector {
    geometry: {
      type: 'Point';
      coordinates: number[];
    };
    properties: {
      names?: string[];
      // Other properties from the referenced containers in your schema
    };
  }

  interface Segment {
    geometry: {
      type: 'LineString';
      coordinates: number[][];
    };
    properties: {
      subType: 'road' | 'rail' | 'water';
      road?: {
        class: string;
        roadNames: Array<string | { at: number[]; names: string[] }>;
        surface: string | Array<{ at: number[]; value: string }>;
        flags: string[];
        lanes: {
          direction: 'forward' | 'backward' | 'both';
          restrictions?: {
            speedLimits?: any; // Define based on the speed limits schema
            access?: any; // Define based on the access schema
            minOccupancy?: any; // Define based on the relational expression schema
          };
        }[];
        // Add more properties based on the schema
      };
      connectors?: string[]; // Array of GERS ID of connectors
      // Add more properties based on the schema
    };
  }


  interface Locality {
    geometry: {
      type: 'Point' | 'Polygon' | 'MultiPolygon';
      coordinates?: number[][][] | number[][];
    };
    properties: {
      subType: 'administrativeLocality' | 'namedLocality';
      localityType: string; // Define the localityType property based on the schema
      names: string[]; // Define the names property based on the schema
      context?: string;
      // Define other properties based on the schema
    };
  }
  
  interface AdministrativeBoundary {
    geometry: {
      type: 'LineString';
      coordinates: number[][];
    };
    properties: {
      adminLevel: number; // Define the adminLevel property based on the schema
      maritime?: boolean;
      // Define other properties based on the schema
    };
  }
  